# ToyRobot

**Table of Contents**

- [Getting Started](#getting-started)
- [Architecture, Design Patterns and Other Documentation](#architecture-design-patterns-and-other-documentation)

## Getting Started

### Environmental setup
- macOS version: High Sierra 10.13.x
- Xcode version: 10.1
- Swift version: 4.2

## Architecture, Design Patterns and Other Documentation

The project has been made using Behaviour-driven-development methodology where-in User Stories are mapped to unit tests first and then code is written in class being tested.

The `Given`, `When` and `Then` of user stories are translated to `describe`, `context` and `it` functions using the Quick testing framework.

The projects also uses protocol oriented programming and dependency injection to make the classes loosely coupled with other classes and makes the code 100% testable.

Please note, The tests are written using Quick, Nimble for the ToyRobot framework and XCTest for the Example App.

The following concepts are used throughout the codebase:

- [Protocol-Oriented with BDD](https://dzone.com/articles/introducing-protocol-oriented-bdd-in-swift-for-ios)
- [MVVM + Router](https://medium.com/commencis/routing-with-mvvm-on-ios-f22d021ad2b2)
- [Dependency Injection](https://cocoacasts.com/nuts-and-bolts-of-dependency-injection-in-swift)
