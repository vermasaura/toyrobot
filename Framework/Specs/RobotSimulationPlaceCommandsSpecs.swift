//
//  RobotSimulationPlaceCommandsSpecs.swift
//  Specs
//
//  Created by Saurabh Verma on 2/6/19.
//  Copyright © 2019 Cognizant. All rights reserved.
//

import Quick
import Nimble
import Result

@testable import ToyRobot

final class RobotSimulationPlaceCommandsSpecs: QuickSpec {
    
    override func spec() {
        
        describe("a client to simulate robot") {
            
            var client: RobotSimulationClientType!
            
            beforeEach {
                client = RobotSimulationClient(upperRightX: 5, upperRightY: 5)
            }
            
            afterEach {
                client = nil
            }
            
            describe("Given no commands yet") {
                
                context("When PLACE is a first command with valid location") {
                    
                    var result: Result<RobotLocation, CommandError>!
                    
                    beforeEach {
                        client.simulate(with: "0 0 N", response: { response in
                            result = response
                        })
                    }
                    
                    it("Then it returns success") {
                        expect(result.value).notTo(beNil())
                        expect(result.error).to(beNil())
                    }
                    
                    it("And it returns the placed location of robot") {
                        expect(result.value!.x).to(equal(0))
                        expect(result.value!.y).to(equal(0))
                        expect(result.value!.direction).to(equal(.north))
                    }
                    
                }
                
            }
            
            describe("Given no commands yet") {
                
                var result: Result<RobotLocation, CommandError>!
                
                context("When PLACE is a first command with invalid location") {
                    
                    beforeEach {
                        client.simulate(with: "2 6 N", response: { response in
                            result = response
                        })
                    }
                    
                    it("Then it returns failure") {
                        expect(result.error).notTo(beNil())
                        expect(result.value).to(beNil())
                    }
                    
                    it("And it returns the failure type") {
                        expect(result.error) == CommandError.outOfBounds
                    }
                    
                }
                
                context("When a command other than PLACE is given") {
                    
                    beforeEach {
                        
                        client.simulate(with: "LMLMLMLMM", response: { response in
                            result = response
                        })
                        
                    }
                    
                    it("Then it returns failure") {
                        expect(result.error).notTo(beNil())
                        expect(result.value).to(beNil())
                    }
                    
                    it("And it returns the failure type") {
                        expect(result.error) == CommandError.neverPlaced
                    }
                    
                }
                
            }

        }
        
    }
    
}
