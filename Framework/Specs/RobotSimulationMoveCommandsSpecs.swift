//
//  RobotSimulationMoveCommandsSpecs.swift
//  Specs
//
//  Created by Saurabh Verma on 2/6/19.
//  Copyright © 2019 Cognizant. All rights reserved.
//

import Quick
import Nimble
import Result

@testable import ToyRobot

final class RobotSimulationMoveCommandsSpecs: QuickSpec {
    
    override func spec() {
        
        describe("a client to simulate robot") {
            
            var client: RobotSimulationClientType!
            
            beforeEach {
                client = RobotSimulationClient(upperRightX: 5, upperRightY: 5)
            }
            
            afterEach {
                client = nil
            }
            
            describe("Given no commands yet") {
                
                context("When MOVE is a first command") {
                    
                    var result: Result<RobotLocation, CommandError>!
                    
                    beforeEach {
                        client.simulate(with: "M", response: { response in
                            result = response
                        })
                    }
                    
                    it("Then it returns failure") {
                        expect(result.error).notTo(beNil())
                        expect(result.value).to(beNil())
                    }
                    
                }
                
            }
            
            describe("Given a valid PLACE command is executed") {
                
                beforeEach {
                    client.simulate(with: "0 0 W", response: { _ in })
                }
                
                context("When a MOVE command is given") {
                    
                    context("And the command try to move out of bounds") {
                        
                        var result: Result<RobotLocation, CommandError>!
                        
                        originalLocation = RobotLocation(x: 0, y: 0, direction: .west)
                        
                        beforeEach {
                            client.simulate(with: "M", response: { response in
                                result = response
                            })
                        }
                        
                        it("Then it returns failure") {
                            expect(result.error).notTo(beNil())
                            expect(result.value).to(beNil())
                        }
                        
                        it("And it returns the failure type") {
                            expect(result.error) == CommandError.outOfBounds
                        }
                        
                    }
                    
                }
                
            }
            
            describe("Given a valid PLACE command is executed") {
                
                var originalLocation: RobotLocation!
                
                beforeEach {
                    client.simulate(with: "2 2 N", response: { _ in })
                }
                
                context("When a MOVE command is given") {
                    
                    context("And the command results within the location bounds") {
                        
                        var result: Result<RobotLocation, CommandError>!
                        
                        beforeEach {
                            client.simulate(with: "M", response: { response in
                                result = response
                            })
                        }
                        
                        it("Then it returns success") {
                            expect(result.value).notTo(beNil())
                            expect(result.error).to(beNil())
                        }
                        
                        context("When the Robot was facing north") {
                            
                            originalLocation = RobotLocation(x: 2, y: 2, direction: .north)
                            
                            it("Then it moves upward") {
                                expect(result.value!.y) == originalLocation.y + 1
                            }
                            it("And it's x position remains same") {
                                expect(result.value!.x) == originalLocation.x
                            }
                            it("And it's direction remains same") {
                                expect(result.value?.direction) == originalLocation.direction
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
            describe("Given a valid PLACE command is executed") {
                
                var originalLocation: RobotLocation!
                
                beforeEach {
                    client.simulate(with: "2 2 W", response: { _ in })
                }
                
                context("When a MOVE command is given") {
                    
                    context("And the command results within the location bounds") {
                        
                        var result: Result<RobotLocation, CommandError>!
                        
                        beforeEach {
                            client.simulate(with: "M", response: { response in
                                result = response
                            })
                        }
                        
                        it("Then it returns success") {
                            expect(result.value).notTo(beNil())
                            expect(result.error).to(beNil())
                        }
                        
                        context("When the Robot was facing west") {
                            
                            originalLocation = RobotLocation(x: 2, y: 2, direction: .west)
                            
                            it("Then it moves to the left") {
                                expect(result.value!.x) == originalLocation.x - 1
                            }
                            it("And it's y position remains same") {
                                expect(result.value!.y) == originalLocation.y
                            }
                            it("And it's direction remains same") {
                                expect(result.value?.direction) == originalLocation.direction
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
            describe("Given a valid PLACE command is executed") {
                
                var originalLocation: RobotLocation!
                
                beforeEach {
                    client.simulate(with: "2 2 E", response: { _ in })
                }
                
                context("When a MOVE command is given") {
                    
                    context("And the command results within the location bounds") {
                        
                        var result: Result<RobotLocation, CommandError>!
                        
                        beforeEach {
                            client.simulate(with: "M", response: { response in
                                result = response
                            })
                        }
                        
                        it("Then it returns success") {
                            expect(result.value).notTo(beNil())
                            expect(result.error).to(beNil())
                        }
                        
                        context("When the Robot was facing east") {
                            
                            originalLocation = RobotLocation(x: 2, y: 2, direction: .east)
                            
                            it("Then it moves to the right") {
                                expect(result.value!.x) == originalLocation.x + 1
                            }
                            it("And it's y position remains same") {
                                expect(result.value!.y) == originalLocation.y
                            }
                            it("And it's direction remains same") {
                                expect(result.value?.direction) == originalLocation.direction
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
            describe("Given a valid PLACE command is executed") {
                
                var originalLocation: RobotLocation!
                
                beforeEach {
                    client.simulate(with: "2 2 S", response: { _ in })
                }
                
                context("When a MOVE command is given") {
                    
                    context("And the command results within the location bounds") {
                        
                        var result: Result<RobotLocation, CommandError>!
                        
                        beforeEach {
                            client.simulate(with: "M", response: { response in
                                result = response
                            })
                        }
                        
                        it("Then it returns success") {
                            expect(result.value).notTo(beNil())
                            expect(result.error).to(beNil())
                        }
                        
                        context("When the Robot was facing south") {
                            
                            originalLocation = RobotLocation(x: 2, y: 2, direction: .south)
                            
                            it("Then it moves downwards") {
                                expect(result.value!.y) == originalLocation.y - 1
                            }
                            it("And it's x position remains same") {
                                expect(result.value!.x) == originalLocation.x
                            }
                            it("And it's direction remains same") {
                                expect(result.value?.direction) == originalLocation.direction
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
