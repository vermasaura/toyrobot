//
//  RobotSimulationLeftCommandsSpecs.swift
//  Specs
//
//  Created by Saurabh Verma on 2/6/19.
//  Copyright © 2019 Cognizant. All rights reserved.
//

import Quick
import Nimble
import Result

@testable import ToyRobot

final class RobotSimulationLeftCommandsSpecs: QuickSpec {
    
    override func spec() {
        
        describe("a client to simulate robot") {
            
            var client: RobotSimulationClientType!
            
            beforeEach {
                client = RobotSimulationClient(upperRightX: 5, upperRightY: 5)
            }
            
            afterEach {
                client = nil
            }
            
            describe("Given no commands yet") {
                
                context("When LEFT is a first command") {
                    
                    var result: Result<RobotLocation, CommandError>!
                    
                    beforeEach {
                        client.simulate(with: "L", response: { response in
                            result = response
                        })
                    }
                    
                    it("Then it returns failure") {
                        expect(result.error).notTo(beNil())
                        expect(result.value).to(beNil())
                    }
                    
                }
                
            }
            
            describe("Given a valid PLACE command is executed") {
                
                var originalLocation: RobotLocation!
                
                beforeEach {
                    client.simulate(with: "2 2 S", response: { _ in })
                }
                
                context("When a LEFT command is given") {
                    
                    var result: Result<RobotLocation, CommandError>!
                    
                    beforeEach {
                        client.simulate(with: "L", response: { response in
                            result = response
                        })
                    }
                    
                    it("Then it returns success") {
                        expect(result.value).notTo(beNil())
                        expect(result.error).to(beNil())
                    }
                    
                    context("When the Robot was facing south") {
                        
                        originalLocation = RobotLocation(x: 2, y: 2, direction: .south)
                        
                        it("Then it's x and y remains unchanged") {
                            expect(result.value?.x) == originalLocation.x
                            expect(result.value?.y) == originalLocation.y
                        }
                        
                        it("And it's direction changes to East") {
                            expect(result.value?.direction) == .east
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}

final class RobotSimulationRightCommandsSpecs: QuickSpec {
    
    override func spec() {
        
        describe("a client to simulate robot") {
            
            var client: RobotSimulationClientType!
            
            beforeEach {
                client = RobotSimulationClient(upperRightX: 5, upperRightY: 5)
            }
            
            afterEach {
                client = nil
            }
            
            describe("Given no commands yet") {
                
                context("When RIGHT is a first command") {
                    
                    var result: Result<RobotLocation, CommandError>!
                    
                    beforeEach {
                        client.simulate(with: "R", response: { response in
                            result = response
                        })
                    }
                    
                    it("Then it returns failure") {
                        expect(result.error).notTo(beNil())
                        expect(result.value).to(beNil())
                    }
                    
                }
                
            }
            
            describe("Given a valid PLACE command is executed") {
                
                var originalLocation: RobotLocation!
                
                beforeEach {
                    client.simulate(with: "2 2 N", response: { _ in })
                }
                
                context("When a RIGHT command is given") {
                    
                    var result: Result<RobotLocation, CommandError>!
                    
                    beforeEach {
                        client.simulate(with: "R", response: { response in
                            result = response
                        })
                    }
                    
                    it("Then it returns success") {
                        expect(result.value).notTo(beNil())
                        expect(result.error).to(beNil())
                    }
                    
                    context("When the Robot was facing north") {
                        
                        originalLocation = RobotLocation(x: 2, y: 2, direction: .north)
                        
                        it("Then it's x and y remains unchanged") {
                            expect(result.value?.x) == originalLocation.x
                            expect(result.value?.y) == originalLocation.y
                        }
                        
                        it("And it's direction changes to east") {
                            expect(result.value?.direction) == .east
                        }
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
}
