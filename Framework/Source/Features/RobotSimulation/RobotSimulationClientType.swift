//
//  RobotSimulationClientType.swift
//  ToyRobot
//
//  Created by Saurabh Verma on 31/5/19.
//  Copyright © 2019 Cognizant. All rights reserved.
//
import Result

public protocol RobotSimulationClientType {
    
    /* Test Input
    * 5 5
    * 1 2 N
    * LMLMLMLMM
    * 3 3 E
    * MMRMMRMRRM
    */
    func simulate(with input: String, response: (Result<RobotLocation, CommandError>) -> Void)

}

public enum CommandType: Equatable {

    case makeGrid(x: Int, y: Int)
    case place(location: RobotLocation)
    case move
    case left
    case right

}

public enum Direction: String {
    
    case north = "N"
    case west = "W"
    case south = "S"
    case east = "E"
    
    var left: Direction {
        
        switch self {
        case .north:
            return .west
        case .west:
            return .south
        case .south:
            return .east
        case .east:
            return .north
        }
        
    }
        
    var right: Direction {
        
        switch self {
        case .north:
            return .east
        case .west:
            return .north
        case .south:
            return .west
        case .east:
            return .south
        }

    }

}

public struct RobotLocation: Equatable {
    
    public let x: Int
    public let y: Int
    public let direction: Direction
    
    public init(x: Int, y: Int, direction: Direction) {
    
        self.x = x
        self.y = y
        self.direction = direction
        
    }

}

public enum CommandError: Error {
    
    case badInput
    case outOfBounds
    case neverPlaced
    
}
