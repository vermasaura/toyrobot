//
//  RobotSimulationClient.swift
//  ToyRobot
//
//  Created by Saurabh Verma on 31/5/19.
//  Copyright © 2019 Cognizant. All rights reserved.
//
import Result

public protocol RobotSimulationClientDelegate: class {
    
    func didUpdateRobotLocation(with location: RobotLocation)

}

public class RobotSimulationClient: RobotSimulationClientType {
    
    public init(upperRightX: Int, upperRightY: Int) {
        
        hasBeenPlaced = false
        upperRight = (x: upperRightX, y: upperRightY)
        
    }
    
    // MARK: Public
    
    // MARK: Properties
    
    public weak var delegate: RobotSimulationClientDelegate?
    
    // MARK: RobotSimulationClientType Conformance
    
    public func simulate(with input: String, response: (Result<RobotLocation, CommandError>) -> Void) {
        
        decodeInput(with: input).forEach {
            
            switch decodeCommand(with: String($0)) {
            case .success(let commands):
                commands.forEach {
                    simulate(with: $0, response: response)
                }
            case .failure(let error):
                response(Result(error: error))
            }
            
            guard let lastKnownLoc = lastKnownLocation else { return }
            delegate?.didUpdateRobotLocation(with: lastKnownLoc)
            
        }
        
    }
    
    // MARK: Private
    
    // MARK: Properties
    
    private var hasBeenPlaced: Bool
    private var upperRight: (x: Int, y: Int)
    private var lastKnownLocation: RobotLocation?
    
    // MARK: Functions
    
    private func decodeInput(with input: String) -> [Substring] {
        
        return input.split(separator: "\n")
        
    }
    
    private func decodeCommand(with command: String) -> Result<[CommandType], CommandError> {
    
        let components = command.split(separator: " ")
        
        if components.count == 2 {
            
            guard let firstComponent = components.first,
                let secondComponent = components.last,
                let x = Int(firstComponent),
                let y = Int(secondComponent)
                else { return Result(error: .badInput) }
            
            return Result(value: [.makeGrid(x: x, y: y)])
            
        } else if components.count == 3 {
            
            guard let firstComponent = components.first,
                let thirdComponent = components.last,
                let x = Int(firstComponent),
                let y = Int(components[1]),
                let direction = Direction(rawValue: String(thirdComponent))
                else { return Result(error: .badInput) }
            
            let location = RobotLocation(x: x, y: y, direction: direction)
            return Result(value: [.place(location: location)])

        }
        
        var commands: [CommandType] = []
        for letter in command {
            
            if letter == "L" {
                commands.append(.left)
            } else if letter == "R" {
                commands.append(.right)
            } else if letter == "M" {
                commands.append(.move)
            } else {
                return Result(error: .badInput)
            }

        }
        
        return Result(value: commands)
        
    }
    
    private func simulate(with command: CommandType, response: (Result<RobotLocation, CommandError>) -> Void) {
        
        guard hasBeenPlaced else {
            
            executeInitialCommand(with: command, response: response)
            return
            
        }
        
        executeFollowingCommand(with: command, response: response)
        
    }
    
    private func executeInitialCommand(with command: CommandType, response: (Result<RobotLocation, CommandError>) -> Void) {
        
        switch command {
            
        case .makeGrid(let x, let y):
            executeMakeGridCommand(with: x, y: y, response: response)
            
        case .place(let location):
            executePlaceCommand(with: location, response: response)
            
        default:
            response(Result(error: .neverPlaced))
            
        }

    }

    private func executeFollowingCommand(with command: CommandType, response: (Result<RobotLocation, CommandError>) -> Void) {
        
        switch command {

        case .makeGrid(let x, let y):
            executeMakeGridCommand(with: x, y: y, response: response)
            
        case .place(let location):
            executePlaceCommand(with: location, response: response)
            
        case .move:
            executeMoveCommand(with: response)
            
        case .left:
            executeRotateCommand(with: .left, response: response)

        case .right:
            executeRotateCommand(with: .right, response: response)
            
        }
        
    }
    
    private func executeMakeGridCommand(with x: Int, y: Int, response: (Result<RobotLocation, CommandError>) -> Void) {
        
        upperRight = (x: x, y: y)
        
    }
    
    private func executePlaceCommand(with location: RobotLocation, response: (Result<RobotLocation, CommandError>) -> Void) {
    
        guard isWithinBounds(x: location.x, y: location.y) else {
            response(Result(error: .outOfBounds))
            return
        }
        
        lastKnownLocation = location
        hasBeenPlaced = true
        
        guard let lastKnownLoc = lastKnownLocation else { return }
        response(Result(value: lastKnownLoc))

    }
    
    private func executeMoveCommand(with response: (Result<RobotLocation, CommandError>) -> Void) {
        
        guard let location = lastKnownLocation else {
            response(Result(error: .neverPlaced))
            return
        }
        
        var newX: Int
        var newY: Int
        
        switch location.direction {

        case .north:
            newX = location.x
            newY = location.y + 1
            
        case .west:
            newX = location.x - 1
            newY = location.y
            
        case .east:
            newX = location.x + 1
            newY = location.y
        
        case .south:
            newX = location.x
            newY = location.y - 1

        }
        
        guard isWithinBounds(x: newX, y: newY) else {
            response(Result(error: .outOfBounds))
            return
        }
        let newLocation = RobotLocation(x: newX, y: newY, direction: location.direction)
        lastKnownLocation = newLocation
        
        guard let lastKnownLoc = lastKnownLocation else { return }
        response(Result(value: lastKnownLoc))
        
    }
    
    private func executeRotateCommand(with rotation: CommandType, response: (Result<RobotLocation, CommandError>) -> Void) {
        
        guard let location = lastKnownLocation else {
            response(Result(error: .neverPlaced))
            return
        }
        
        let newDirection = rotation == .left ? location.direction.left : location.direction.right
        let newLocation = RobotLocation(x: location.x, y: location.y, direction: newDirection)
        lastKnownLocation = newLocation
        
        guard let lastKnownLoc = lastKnownLocation else { return }
        response(Result(value: lastKnownLoc))
        
    }
    
    private func isWithinBounds(x: Int, y: Int) -> Bool {
        
        return  x >= 0 && x <= upperRight.x && y >= 0 && y <= upperRight.y
    
    }

}
