//
//  Specs.swift
//  Specs
//
//  Created by Saurabh Verma on 9/7/19.
//  Copyright © 2019 Cognizant. All rights reserved.
//
import XCTest
import ToyRobot
import Result

@testable import ToyRobot_Example

protocol RobotSimulationViewModelSpecsScenarios {
    
    func testExecuteWithSuccessScenario()
    func testExecuteWithFailureScenario_NoInput()
    func testExecuteWithFailureScenario_BadInput()
    
}

final class RobotSimulationViewModelSpecs: XCTestCase, RobotSimulationViewModelSpecsScenarios {

    var client: RobotSimulationClientStub!
    var viewController: RobotSimulationViewControllerStub!
    var viewModel: RobotSimulationViewModel!
    
    override func setUp() {
        super.setUp()
        client = RobotSimulationClientStub()
        viewModel = RobotSimulationViewModel(with: client)
        viewController = RobotSimulationViewControllerStub()
        viewModel.delegate = viewController
    }

    override func tearDown() {
        super.tearDown()
        client = nil
        viewModel = nil
        viewController = nil
    }
    
    // MARK: RobotSimulationViewModelSpecsScenarios Conformance

    func testExecuteWithSuccessScenario() {
        
        // Given
        let input = "5 5\n1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM"
        client.shouldReturnSuccess = true

        // When
        viewModel.executeCommand(with: input)
        
        //Then
        XCTAssertTrue(viewController.timesUserCommandDidSuccessCalled > 0)
        
    }
    
    func testExecuteWithFailureScenario_NoInput() {
        
        // Given
        let input = ""
        client.shouldReturnSuccess = false
        
        // When
        viewModel.executeCommand(with: input)
        
        //Then
        XCTAssertTrue(viewController.timesUserCommandDidFailCalled == 1)
        XCTAssertTrue(viewController.responseMessage == "Command Error! Input can't be empty.")
        
    }
    
    func testExecuteWithFailureScenario_BadInput() {
        
        // Given
        let input = "5 5 5\n1 2 N\nLMLMLMLMM\n3 3 E\nMMRMMRMRRM"
        client.shouldReturnSuccess = false
        client.commandError = .badInput
        
        // When
        viewModel.executeCommand(with: input)
        
        //Then
        XCTAssertTrue(viewController.timesUserCommandDidFailCalled == 1)
        XCTAssertTrue(viewController.responseMessage == "Command Error! Bad Input.")

    }

}

final class RobotSimulationViewControllerStub: RobotSimulationViewModelDelegate {
    
    init() {}
    
    var timesUserCommandDidFailCalled = 0
    var timesUserCommandDidSuccessCalled = 0
    var responseMessage: String?
    
    func userCommandDidFail(with command: String) {
        timesUserCommandDidFailCalled += 1
        responseMessage = command
    }
    
    func userDidAskForReport(with report: String) {
        
    }
    
    func userCommandDidSuccess(with command: String) {
        timesUserCommandDidSuccessCalled += 1
        responseMessage = command
    }
    
}

final class RobotSimulationClientStub: RobotSimulationClientType {
    
    var shouldReturnSuccess: Bool!
    var commandError: CommandError!

    func simulate(with input: String, response: (Result<RobotLocation, CommandError>) -> Void) {
        
        if shouldReturnSuccess {
            response(Result(value: RobotLocation(x: 1, y: 2, direction: .north)))
        } else {
            response(Result(error: commandError))
        }
        
    }
    
}


