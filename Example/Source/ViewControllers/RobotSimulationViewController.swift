//
//  RobotSimulationViewController.swift
//  ToyRobot-Example
//
//  Created by Saurabh Verma on 31/5/19.
//  Copyright © 2019 Cognizant. All rights reserved.
//

import UIKit

final class RobotSimulationViewController: UIViewController {

    // MARK: Initialization
    
    init(with viewModel: RobotSimulationViewModelType) {
        
        self.viewModel = viewModel
        super.init(nibName: String(describing: RobotSimulationViewController.self), bundle: nil)

    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("use init(viewModel:) instead")

    }
    
    // MARK: Private
    
    // MARK: Properties
    
    private let viewModel: RobotSimulationViewModelType
    
    @IBOutlet private weak var input: UITextView!
    @IBOutlet private weak var commands: UITextView!
    @IBOutlet private weak var output: UILabel!
    
    // MARK: Functions
    
    @IBAction private func commandButtonAction() {
        
        input.resignFirstResponder()
        viewModel.executeCommand(with: input.text)

    }
    
    @IBAction private func clearButtonAction() {
        
        input.text = nil
        commands.text = nil
        output.text = nil
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        input.resignFirstResponder()

    }

}

extension RobotSimulationViewController: RobotSimulationViewModelDelegate {
    
    func userCommandDidSuccess(with command: String) {
        
        commands.text = "\(commands.text ?? "") \n\(command)"

    }
    
    func userDidAskForReport(with report: String) {
        
        if let output = output.text, !output.isEmpty {
            self.output.text = "\(output)\n\(report)"
        } else {
            self.output.text = report
        }
        
    }
    
    func userCommandDidFail(with command: String) {
        
        commands.text = "\(commands.text ?? "") \n\(command)"
        
    }
    
}
