//
//  AppDelegate.swift
//  ToyRobot-Example
//
//  Created by Saurabh Verma on 31/5/19.
//  Copyright © 2019 Cognizant. All rights reserved.
//

import UIKit
import ToyRobot

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Dependencies Injection
        let robotClient = RobotSimulationClient(upperRightX: 5, upperRightY: 5)
        let viewModel = RobotSimulationViewModel(with: robotClient)
        robotClient.delegate = viewModel
        let viewController = RobotSimulationViewController(with: viewModel)
        viewModel.delegate = viewController
        let navigationController = UINavigationController(rootViewController: viewController)
        
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
        
        return true
        
    }

}

