//
//  RobotSimulationViewModelType.swift
//  ToyRobot-Example
//
//  Created by Saurabh Verma on 2/6/19.
//  Copyright © 2019 Cognizant. All rights reserved.
//

protocol RobotSimulationViewModelType {
    
    func executeCommand(with input: String?)
    
}
