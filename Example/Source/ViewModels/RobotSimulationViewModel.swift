//
//  ViewModel.swift
//  ToyRobot-Example
//
//  Created by Saurabh Verma on 2/6/19.
//  Copyright © 2019 Cognizant. All rights reserved.
//

import ToyRobot

protocol RobotSimulationViewModelDelegate: class {
    
    func userCommandDidSuccess(with command: String)
    
    func userCommandDidFail(with command: String)
    
    func userDidAskForReport(with report: String)
    
}

final class RobotSimulationViewModel: RobotSimulationViewModelType {
    
    weak var delegate: RobotSimulationViewModelDelegate?

    // MARK: Initialization
    
    init(with robotClient: RobotSimulationClientType) {
        
        self.robotClient = robotClient
        
    }
    
    // MARK: RobotSimulationViewModelType Conformance
    
    func executeCommand(with input: String?) {
        
        guard let command = input, !command.isEmpty else {
            self.delegate?.userCommandDidFail(with: "Command Error! Input can't be empty.")
            return
        }
        
        robotClient.simulate(with: command) { [weak self] result in
            
            guard let self = self else { return }
            
            switch result {
                
            case .success(let value):
                self.delegate?.userCommandDidSuccess(with: "Command: \(value.x, value.y, value.direction.rawValue)")
                
            case .failure(let error):
                self.delegate?.userCommandDidFail(with: self.handleError(with: error))
                
            }
            
        }
        
    }
    
    // MARK: Private
    
    // MARK: Properties
    
    private let robotClient: RobotSimulationClientType
    
    // MARK: Functions
    
    private func handleError(with error: CommandError) -> String {
        
        switch error {
        case .neverPlaced:
            return "Command Error! Robot has not been placed yet."
        case .outOfBounds:
            return "Command Error! Robot asked to go out of grid."
        case .badInput:
            return "Command Error! Bad Input."
        }
        
    }

}

extension RobotSimulationViewModel: RobotSimulationClientDelegate {
    
    func didUpdateRobotLocation(with location: RobotLocation) {
        
        delegate?.userDidAskForReport(with: "Output: \(location.x, location.y, location.direction.rawValue)")
        
    }
    
}
